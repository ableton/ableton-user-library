Download the latest of everything in this repo here:

* https://gitlab.com/ableton/ableton-user-library/-/archive/master/ableton-user-library-master.zip

And read this *readme* file below - the clues are often in the file names:

# Introduction

Welcome to the gift for your future-self, in being organised by design, so that being organised needs less organising and, well *time is creativity* - it's much more fun creating, than wondering where that neat sound you had somewhere went to. Why?

1. It seems to take eons of time to design presets, instruments and templates, but then it saves much more time later, for yourself and your collaborators, from repeating processes or reorganising for later stages of production to publishing - and we all want to generally work time-efficiently, avoid re-inventing wheels or endless reading, and use audio signal-flow best-practices that any reasonably versed artist or producer should find familiar and useful too.
2. Since discovering Ableton Project .als files are simply GZipped .xml files (familiar to web and software developers) - it made sense to me that they could be put onto Git version-control (web & software developers use Git a lot, but it's handy for anyone collaborating on software files to know about) to make the visibility of changes transparent and documented, so here we are - it should save you the hundreds of hours that it took me to research and design these templates and presets.
3. There's almost infinite complex, contradicting and intimidating information sources out there in interwebland - but thankfully I studied Audio Engineering at Uni when the internet was a mere side-show, and I got to spend a bunch of time working in analogue studios back in the day, learning with ProTools - so I have a general feeling for the signal flow and well-worn processes that are familiar and common throughout the music recording & publishing industry. I've aimed for creating standard best-practice defaults so that you have a reference for what the general rules look like - you'll know when to rely on them, and can try breaking them but still be able to compare what you're doing without. There's nothing wrong with fishing for happy accidents but it can also risk frustratingly un-mixable unhappiness if you don't follow the basics, like composing to -12dB for mixing to -6dB, for mastering to 0dB - instead of using the default mode of everything at 0dB for example: https://sonicbloom.net/en/gain-staging-in-ableton-live-done-right/
4. And there's not much point in us all researching the same standard things that are generally common to all recording and composing needs, when open-source can make all that time, knowledge and experience cumulative. Although Git is traditionally a software developer tool, don't be put-off by all the text and files, actually Git(Lab) is designed for this kind of collaborative data and simplified content sharing and evolution, regardless of whether programming software code, writing prose or just sharing files - so you'll probably see today's kids using this method of working more in any industry that involves information sharing, collaboration and evolution, all with a handy audit-trail of who did what and why to learn from. It's actually the same thing that Splice Studio (https://splice.com/features/studio) and similar do anyway - but a free open-source alternative! And interesting to know what does on under the hood sometimes.
5. Anyway, don't worry about where it all came from - just click Download > Zip in the top-right of this repo, unzip and place somewhere like `~User > GitLab > ableton-user-library`. It's all your open-source gift from me, and as free as a bird.

In *theory* you should just be able to use the Ableton *User Library* from this download as your main starter (or secondary reference). Start with a 16-track template for your version of Ableton, and get stuck into making tunes in a way almost any assistant producer should appreciate too! All with a library of presets that are named to make some sense of what they can do, and you can then make something sweet and musical with an amount of organising and standard production-technique thinking and signal routing already done for you.

You don't *need* to use the Template files for starting projects, and can just drag the tracks you want from the Template tracks - but you'll need to recreate the Return Routing if you're using Returns to consolidate effects because you can't copy Return channels from the Templates file-tree. The Templates are a good reference to start with though, and you can just delete what you're not using, but knowing where things should typically be in the signal chain if you need to add something back. It's probably easier to start with these templates first until you know what everything does, so you can be more focused and compare to any reference-track Ableton Templates you may purchase to learn from too (links to more of those below).

It's worth noting, the Template has everything you could possibly imagine using in it - but - that takes a bit longer to open initially, so once you know what you want, feel free to delete all that you don't for a leaner file opening time.

# Installation

This project contains a generic *User Library* folder template that any Ableton user may find useful. Us it as either as a nicer starter than the empty folders installed by default (the default folder structure remains cloned and respected here too - so you're not losing anything, it's just extended a little in anticipation of common needs, to include all the right files in the right places as a whole and portable collection) - or you can just use this as a reference folder for ideas to add or evolve your own existing *User Library*. The other folders and files in this repo are just for documentation reference.

Get to know, and *own*, your Ableton *User Library* folder structure! Ultimately, it is your curated toolkit that will speed up your own creative revolution - it will eventually become part of your overall sound as you reuse and remix from it too.

Simply download the entire project, and (if replacing your own with this, make sure to take rename of your own *User Library* folder `User Library-old` or something first) to either replace you entire *User Library* folder with this one if you've just started out and never put anything of value in your existing *User Library* folder - or keep it in `~User > GitLab > ableton-user-library > User Library - GitLab` and add it through `Places` in Ableton, to use as a secondary option for these tools and templates. That way, when there's updates here, you can just download and overwrite that secondary `User Library - GitLab` folder with the latest from here - or use GitHub Desktop as described below to synch it to the latest repository here.

# General Research Notes

## Ableton

* It makes sense to keep these Templates & Presets all in one project Git repository, regardless of whether you have Ableton Intro, Studio or Suite - both to save duplication of effort for me in creating and uploading, and to be readily available should upgrading take your fancy. I'll aim to make everything labelled to show if there are different Intro or Studio/Suite versions - but fundamentally they tend to do the same thing with more or less plugins.
* Almost everyone should get familiar with Ableton using Intro first, it forces you to be creative within the 16 (stereo) tracks and 2 (stereo) returns, to use the Instrument, Drum & Effect Racks as they were intended - and focuses you on everything that is provided in Intro that is more that sufficient to write great music with - and you can still have unlimited instruments and effects within a track using the Drum & Instrument Racks anyway - again, important to learn how to use those racks, which then become your favourite presets saved into your *User Library* treasure-chest.
* Studio & Suite are simply just for a wider selection of effect, instruments, presets and packs - but if you are a singer, instrument musician, sampling or synth plugin composer, you don't really need the more expensive versions to get started and make great music - try to work with just Intro, 16 tracks & 2 Returns for as long as possible until you are completely sure that Ableton is for you, and you're passionate about spending more time with it as an instrument in its own right.
* If you're just using Ableton as a more of a live-performance and simpler alternative to Logic, ProTools or other DAWs, then you may also find Intro does all you need for a very long time with minimal overwhelming-choice distractions anyway, and might keep you from running your CPU so high if you don't know how to be lean with just having what you need for reliable performance from almost any Mac made in the last 10 years should handle.
* If you decide to do your own mastering or want to go crazy with tracks and sounds, then Suite is great value compared to all the separate plugins and sounds you might buy to do similar - but you can always send projects for mastering to someone with Suite and they will still work perfectly - and there's a load of free equivalent VST plugins out there if money is tight or needed more elsewhere.
* Compare Live versions here:
  * https://www.ableton.com/en/live/compare-editions/

# Shopping Notes

## Plugins

* Default Ableton plugins are used in the Template Projects included - this is to show the typical rack signal chain for each sound or effect-type in a way all Ableton versions will have, so you can drop-in and replace with your other swanky plugins to do similar things if you so desire - but try the Ableton ones first, they are all sweet sounding, and keep your powder dry to reserve limited funds for shiny instruments and inspirations.

### Effects

* The Soundtoys collection of plugins are used to demonstrate 3rd-party effects positioning, I like these, you may too or you may like other's, there's plenty of free or alternative options, probably depends a little on your budget and style but I find them good for quite a variety of genres and sounds, so you can't really go wrong if you can afford - probably the whole bundle, and ideally when they are on sale ;) But, still start with the included Ableton plugins first, which includes pretty much one of every type of plugin you could imagine anyway. If you don't have, want or need the Soundtoys plugins included in tracks, just ignore, delete or replace them as you wish, and make copy templates without them or with whatever else similar you like if you prefer.
  * https://www.soundtoys.com
  * https://www.pluginboutique.com?a_aid=5e0caa34b1843&amp;a_bid=f271f4a1 (+5% Rewards discounts)

### Instruments

#### Soft-Synths

* If you're looking for that one dynamic synth collection for a reasonable budget, the Arturia V Collection is great quality, authentic synthesis (not sampled sounds), variety and value - especially if you get it on sale at key times of year at 50% off or with a discount in upgrading from the lite version included with their awesome hardware. Pigments looks pretty good too, and is discounted if you have any other Arturia product. Their hardware is all great too, and you can mostly get the other synth-brand sounds you might like from their soft-synth versions.
  * https://www.arturia.com/products/analog-classics/v-collection/overview#en
  * https://www.arturia.com/products/analog-classics/pigments/overview#en
  * https://www.arturia.com/products
  * https://www.gear4music.com
* You can get pretty much get all the Korg signature sounds as authentic soft-synths too, so you can work with any keyboard controller you like the feel and layout of without having to budget for Korg hardware as well:
  * https://www.korg.com/us/products/software/korg_collection/
* Many modern templates and sounds use Native Instrument's Massive, so if you just want one omni-purpose synth for modern music, there's plenty you can do with that, and expand it with widely available additional presets.
  * https://www.native-instruments.com/en/products/komplete/synths/massive/
* Sylenth is very common on modern electronic music and templates, and great for electrifying other genres.
  * https://www.lennardigital.com/sylenth1/
* Xfer Serum is possibly the most popular sound generator on modern tracks and templates, often used to recreate sounds from any other electronic instrument too:
  * https://xferrecords.com/products/serum

#### Hardware Synths

* For top-of-the-range performance, have a look at Nord or Yamaha:
  * https://www.nordkeyboards.com
  * https://www.yamahasynth.com
* Bang for bucks, Arturia, Sequential or Casio:
  * https://www.arturia.com/products
  * https://www.sequential.com/product-category/instruments/
  * https://www.casiomusicgear.com

#### Soft-Instruments

* Almost everything you can imagine is offered by Native Instruments Komplete Collections - with a budget to match of course - but sometimes there's bundle deals with their complementary integrated keyboards with key-lights:
  * https://www.native-instruments.com/en/catalog/komplete/
  * https://www.native-instruments.com/en/catalog/komplete/keyboards/
* UVI has a great collection of sampled classic synths. Pros: wide selection of sounds recorded from the originals for accurate reproduction and maybe less CPU usage since they are mostly playing back recorded audio. Cons: a lot more more disk-space and they aren't programatic sound synthesis, so less options to create new sounds.
  * https://www.uvi.net/en/vintage-synth
* Superior Drummer is a drummer's delight, if you're into the details & nuances - although try the Drummer Packs included with Native Instruments Komplete first if you have it:
  * https://www.toontrack.com/product/superior-drummer-3/
  * https://www.native-instruments.com/en/catalog/komplete/drums/

## Audio Interfaces

* Try and stick to USB3 for more portability if you change other parts of your setup later. Get one with ADAT if you expect to want to add more Audio Inputs later.
* Top-notch - Focusrite
  * https://focusrite.com/en/usb-interfaces
  * https://focusrite.com/en/octopre (to add an additional 8 Audio inputs via an ADAT optical cable link.)
* Live playback - iConnectivity (although Focusrite may be able to do all of the same redundancy things for live too with a thoughtful MIDI setup)
  * https://www.iconnectivity.com
  * https://www.iconnectivity.com/featured-artists/2019/2/28/billie-eilish
* On a budget - Gear4Music tend to offer the most popular alternatives and brands. Check review via Google & Amazon but try to support a specialist website with your good custom for a more knowledgable product-specific service if prices are similar.
  * https://www.gear4music.com/Soundcards/USB.html

## Thoughts

* You really can make almost any kinds of music already with all that and maybe your own recordings or sample packs. Save your money and stick with the cheapest things first, until you really know if must have those more nuanced or stylised sounds to compliment them.
* Find some Ableton reference tracks from the links at the bottom of this list to study how your favourite styles are composed, and what instruments & effects they might be using. They are probably simpler than you think, and can be a good fun starting point for rearranging, remaking, changing instruments, beats, vocals etc.
* Hindsight is a wonderful thing too, all this is mine.

# Ableton Notes

## Collections

It is handy to name your collections with the same number-series as their shortcut number keys, for quickly tagging things for that sound-type with each colour using your keyboard as you navigate all the many things there are to discover and remember, simply select and tap that number on your computer keyboard.

My personal Ableton Collections are named as follows:

* `#FF0000` 1. Drums
* `#FFA500` 2. Orchestra
* `#FFFF00` 3. Keys
* `#00FA9A` 4. Vocals
* `#1E90FF` 5. Bass & Guitar
* `#9370D8` 6. Synth
* `#C0C0C0` 7. SFX & Mix

This colour-series is specifically chosen to approximate the same colour-coding system used in the popular and useful `Samplism` application too, for ease of having a common navigational intuition because, well, *time is creativity*:

* https://audiohelperproject.com/products/samplism/

If you want to follow my Collection colours, add the `User Library > Ableton Folder Info` folder content from here into your own. If not, then just don't copy that folder and stick to what you already do.

You'll notice in the Templates I follow the same track rainbow colour-scheme and order of sounds - the projects are orderly so your creativity can be a random exploration - try starting new tracks with a melody, pad, arpeggiator or whatever, there's no rules for originality - the enjoyment of the game isn't the lines on the field but what you do with them.

## Preferences

* https://gitlab.com/ableton/ableton-user-library/-/tree/master/Ableton%20Preferences

Screenshots of the Ableton *Preferences* that your good author has are included for reference, just in case you are unsure and the rollover help text isn't speaking your language yet.

* If you expect for your music to ever be synched to a music video performance of drums or vocals, the video editor will be very happy with you for exporting with a 48,000 Sample Rate, since it is a useful multiple of common film & video frame-rates to help make synchronising precise audio less likely to have latency and speed-matching issues.
* If you are recording with the same video-synching intentions - record in 44.1kHz & 24bit, and export in 48kHz & 16bit - effectively the same data but each matching typical conventions you'll find work for almost anyone.
* If you intend to do extensive audio manipulation and quality preservation is a concern, or you will need to export at a higher sample-rate, just be sure to use 24bit by default to avoid digital loss on export.
  * Note: most sample libraries come as 44.1kHz & 24bit, which should export as 48kHz & 16bit with no discernible loss of quality if you are exporting with the aforementioned video tracking expectation or for mastering. Audio mastering shouldn't mind either way but probably will find a matching 44.1kHz & 24bit is welcome.
* Turn off Audio Input & Outputs you're not using to save some CPUs, just turn them on when you need. You don't need Audio Inputs selected unless you're actually recording, and you may be surprised how CPU-efficient Ableton can be on even ageing computers and laptops when you just only enable what you need, when you need it. This goes for Preferences, and Racks - when that orange light on a Rack is on it is using CPU, click it to go grey and off to save CPU if you're not using something.

## Options

* https://gitlab.com/ableton/ableton-user-library/-/blob/master/Options.txt

The Options.txt file included is my personal setup for workflow speed. It disables plugin scanning on every launch, automatically sets the track selected to be MIDI armed, and a couple of other handy things. You don't *need* to use it, but I find it makes things faster. Feel free to use this or edit your own to match. More info on how to do that here:

* https://help.ableton.com/hc/en-us/articles/209772865-Options-txt-file

This does make Ableton start a lot faster too by skipping the superfluous plugin folders scan each time - but that also means when you install or update any plugins, you need to go to Ableton > Preferences > Plug-Ins > Rescan for them to be recognised.

If you ever have any plugin crashing issues, try Alt-clicking Rescan for a full plugin folder rescan, which should then show any plugins that recently caused a crash for you to review if the plugin is the problem, or the version you are using, so you can try a different (AU/VST2/VST3) version or seek an update or vendor support. Remember, only ever use one version of any plugin in the same project, as Ableton advises too:

* https://help.ableton.com/hc/en-us/articles/209068929-Using-AU-and-VST-plug-ins-on-Mac
* https://www.beatlabacademy.com/9-ableton-live-fundamentals/

## General Ableton Maintenance Advice

### Plugin Locations

#### Mac

* AU - `Macintosh HD:Library:Audio:Plug-Ins:Components`
* VST2 - `Macintosh HD:Library:Audio:Plug-Ins:VST`
* VST3 - `Macintosh HD:Library:Audio:Plug-Ins:VST3`

I personally add *aliases* (drag-n-drop holding alt+cmd) to those folders into a `Macintosh HD:Users:username:Music:Plugin Folders` folder, for quick-access in keeping them clean & lean by removing unwanted VST2 versions.

#### Windows

* https://help.ableton.com/hc/en-us/articles/209071729-Using-VST-plug-ins-on-Windows

### Plugin Tips

#### Which plugin versions to use?

Just like websites, the technology, presentation, efficiency and thoughtfulness of software development varies wildly. Some developers are focused just on the information or end result, and some have larger organisations and resources to polish the package for all types of users. Some developers made something great a long time ago with older technologies, and just haven't had the resources to change later as new development frameworks can sometimes mean a complete rebuild, which is just not always fun when you've already done something once. So they may have compromises like only supporting one platform, not having scaling interfaces, not work with other things etc. You just have to start with the best of the bunch, like Ableton and Arturia first, then later as your appetite for variety and niche products might grow to solve particular ambitions, these organisation foundations will keep your time spared from troubleshooting or difficult to maintain and migrate setups. Hardware and software can both fail, so especially with live performance and certainly if it is your income, it's a good investment to understand these things now and clean as you go to save from later frustrations. On a good note, old technologies that have stood the test of time are tried, tested and battle-hardened, so don't dismiss something just on it's interface, if it works now, it will probably work for a very long time still without issue.

* Use VST3 versions when available, VST2 when there's no VST3 version released by the vendor yet, and AU if there's no VST versions available at-all. Have this same preference when tagging your Plugins  with *Collection* colours. Avoid tagging multiple versions of the same plugin, just tag the highest preference version, it'll keep your *Collections* nice and unconfused.
* Mac users can install, but then safely avoid using AU plugin versions, unless you really need them, for example: when working with a project file from someone on a Mac that does use AU or a mixture - otherwise it is better to turn off AU plugins completely in *Preference* though, then you'll avoid accidentally using from searches - and it keeps your work transportable to collaborate with Windows users. Plus, it keeps your *User Library* more predictable, and searches won't have duplicate results that might mean you use the same plugin in different formats in the same project with unreliable results, as advised by Ableton too.
  * There's no harm in having all versions installed and using the default install options, it's just something worth knowing about and keeping organised whichever way you prefer because you'll find it much easier to understand and fix any conflict issues if you're aware of these little details in keeping your setup organised and somewhat understood.
* When installing plugins - just install the AU and VST3 versions only, and make a habit of doing that - VST3 & AU use a little less less CPUs when not in use and generally can open and close a fraction quicker - Ableton doesn't use AAX or RTAS at all, so you don't need them unless you also use DigiDesign's Avid or ProTools.
  * An exception to this is if you want to use the AKAI VIP or Native Instruments Kontakt and Komplete Kontrol software for browsing, cataloging and playing VSTs outside of your DAW or as a Plugin wrapper on MIDI tracks because they only support VST2 versions at this time. So if you are installing VST2 and VST3 versions for this, just try to make sure you tag just the VST3 plugins with Ableton Collection Colours to make it easier to identify them in searching and browsing, so that you're only using the VST3 version. This avoids conflicts and makes sure your saved Ableton Plugin Presets always work, since they only work on the version they were saved using.
* Usually there is a "Customise" option in the installer, always review the options there, and you can safely untick AAX & RTAS - and VST/VST2 if there is also VST3 and you aren't using AKAI VIP or Kontakt (so that leaves you with AU & VST3 OR VST/VST2 selected, and any other optional extra files & presets you want, usually all those ticked by default).
* If you have any Arturia licences, in the Arturia Software Centre, go to Preference > Advanced > Untick "Silent install" - this way you'll get the option to customise which plugin versions to install or update each time there's updates, this saves re-deleting the unwanted VST/VST2 & AAX ones each time.
* The reason for still installing the AU versions is for if you work with another Mac user that uses them, they will still be quickly available to use if you need because VST and AU Presets only work on either the VST or AU they were create with, but not both. You can still have AU plugins installed and just leave AU plugins off in your *Preferences* - but they will be there in case you do need them to save you re-running installers later.
* If you've already installed a bunch of plugins before reading all this, you can remove all duplicate VST2 plugins from your system's VST plugins folders and just use VST3 versions in future if they are working without any issues - it saves accidentally using different versions of the same plugin, which can cause crashes or other unwelcome side-effects. VST/VST2 Presets all still work with the remaining VST3 Plugins, and all projects opening that had a VST/VST2 plugin version used before will recognise and use the remaining VST3 version instead.
* There are still many plugins that are VST2-only, like Soundtoys, and they are equally capable, we're just avoiding the VST2/3 conflict potential through being organised and aware of the habit for having and reasoning a preference since these multiple options can seem confusing when just getting setup.
* When running updaters, they can re-install VST2 and other unwanted versions, so if the installer doesn't show options, you can check those folders again to remove them if you didn't run the installer and customise the installation to untick them.
* If you do have a VST3 plugin that causes crashes, try removing it and installing only the VST2 version. The sound quality and functionality is the same, it only *might* use a tiny bit more CPU. It's rare nowadays, and usually fixed later with updates, so worth re-trying any you have to do this with future plugin has updates to see if the issue is now solved.
* If you don't use Logic, Garageband, FinalCut or Protools etc and don' collaborate, you *can* also remove AU and AAX - be confident you know that you don't need them if you are just using Ableton and not collaborating with anyone that hasn't read all this advice and still uses AU versions, but you should be able to install or re-install anything later if you need it - just focus on using Ableton's native Audio Effects, Instruments and VST3 Plugins first where possible for a generally easier life, regardless of if you clean up your plugin folders or not, it's not the end of the world, as if you do have an issue you can just come back and re-read this to try and figure out why and how to troubleshoot with some knowledge of where to start.

#### Plugin custom install recommended options to un-tick

![Plugin Installer Custom Options](https://gitlab.com/ableton/ableton-user-library/-/raw/master/Other%20Screenshots/Plugin%20Custom%20Install%20Recommendations%20Screenshot.png "Plugin Installer Custom Options")

#### Saving CPU & Performance

* Think about having a separate user login just for your music making if you also use your computer for other things like work, correspondence, shopping etc. Maybe call it `Playtime`, or whatever your stage name is. This way you can switch user logins to get in the zone and focus, without other internet distractions, and keep your main user account free from music making stuff to also stay uncluttered in getting through your day-job and life-admin. Try and set-up the bare minimum of internet-connected apps in your `Playtime` login to keep it all super responsive to just the music stuff - and doubly important if you're going to be using your computer for stage performance too! Migrating an existing setup to a new user login can be an amount of work re-licencing and re-setting file paths, so if you're already happy with your setup, maybe create the separate login for just your work and life admin stuff as the software you need for that is most-likely much quicker and easier to reorganise in the new profile.
* Turn off all Audio Racks (that bottom row of mini-windows with the orange/grey button in the top-left for on/off) if you're not using them - that saves some CPUs - all Racks in these Presets and Templates should be off by default when you open them for that reason - it also helps you just turn on what you need and see if it can do what you are hoping for - one tweak at a time keeps a sane mind and workflow - and you'll gain an ear for what each thing does.
* Run backups at times you're not working.

##### AppTamer

* Mac users can install "AppTamer" to freeze CPU hungry background apps so the Application in front is uninterrupted. Install, go to its Preference, set to Open at Login.
  * https://www.stclairsoft.com/AppTamer/

#### Saving time

* Get to know the *Collections*, *User Library* and *Places* features - they help you to keep organised, and working quickly with all your favourite sounds and setups - eventually becoming a setup that is your sound.
* Expand .als files in the *User Library* tree view, Packs or Places, to see the tracks within a Project, and then drag in copies of any tracks from your Template files, or other favourite Projects, to re-use or adapt from them.
* Double click on a Template file to create a new Project based on that Template - when you go to save, you will be making a new version in your well-organised `~User > Music > Ableton > Projects` treasure-trove of original and exclusively-yours aural delights.
* Happy with a loop or segment of a track? Drag it to your User Library Clips folder for quick re-use in other projects.

#### Saving chaos

* Save all *versions* of the same .als project in that same folder (read that again *versions*, eg: `Wickedness 1.als`, `Wickedness 2.als`, all within a `Wickedness` project folder). This avoids duplicating all the dependant files in that same project folder, so you are just versioning project files but not project folders!
* Use number versioning with a space and number after the file name to experiment and evolve safely, with previous versions still available to return to or draw from if you need, so your current version can remain lean and focused, knowing anything you delete or experiment with in new versions, you can recover again from older versions.
* If you use an external drive to store stuff, just call it `Sound Library` or whatever you're happy to keep consistent, you need to stick to that name as it will save you work by keeping the same file-paths if ever you need to move to a larger or faster external drive or need to restore a backup in the event of losing one - which I hope never happens to you, but hard drives can be delicate, especially non-SSD ones or those not kept in a safe place and in a coffee-spillage-proof case ideally. Many of the things you use will have this path saved and will need preferences or indexes updating if you change it, which is usually easy enough but is all time interrupting the creative flow.
* I personally use a 2TB external SSD drive (SanDisk Rugged) for storing all the vast amounts of data that plugin sounds, sample packs and other downloads can take up - but then keep the actual Live Projects and their contents (Using `File > Collect All & Save` as a habit) on the main hard drive, so they still work without the external drive plugged-in for saying playing with projects on trains & planes, but you might also want to keep all that on an external drive too for portability between machines, either way will usually perform fine and is OK, the point is to be organised and have just one place to look for things, and of course, keep the most important things that you would want to recover from any disasters backed-up!
* Within my external `Sound Library` drive I have the following folder structure, that should be relatively static and scaleable over time and is used for the top-level folders to be added as `Places` in Ableton, `Libraries` in Samplism, Sononym, and the Pack folders in Loopcloud all for consistency in finding and tracing sources:
- `Sound Library` *(this is the name of your external drive)*
  - `Ableton Packs`
    - `Factory Packs` *(set this location in Ableton Preferences)*
    - `Project Packs`
      - `abletunes.com`
      - `loopmasters.com`
      - `topmusicarts.com`
      - etc
  - `MIDI Packs`
    - `off-the-beat.com`
    - etc
  - `Plugin Sounds`
    - `Kontakt Instruments` *(set this location in Native Instruments Preferences, keep them all in the same folder, whether from NI direct or 3rd-parties)*
    - `ToonTrack` *(set these locations in Superior Drummer Preferences)*
      - `EZDrummer`
      - `Superior Drummer`
      - etc
  - `Remix Packs` *(keep these resources separate from your licensed `Sample Packs` so that you have a clear distinction for anything you might need to seek licensing permissions and agreements for publishing.)*
    - `acapellas4u.co.uk`
    - `reddit.com`
    - `rhythm-lab.com`
    - `youtube.com`
    - etc
  - `Sample Packs` *(of samples, presets and templates - use domain names to remember what came from where, and proof of that in the event of any license or source queries)*
    - `loopmasters.com`
    - `platinumloops.com`
    - `primeloops.com`
    - `producerloops.com`
    - `producerplanet.com`
    - `reddit.com`
    - `rhythm-lab.com`
    - etc
  - `Installers` *(keep them if you want, I tend to as I don't install everything straight away when trialing things or gathering free stuff, and just delete each when done or later if you're short on space)*
  - `Sampling` *(keeping the Clips that I sample separate from the `Sample Packs` of downloaded samples, presets and templates packs)*
  - `Spotify Converter` *(for downloaded full reference tracks)*
  - `Tutorials`
* Keep your own projects on your main Hard Drive, usually in the default folder `~User > Music > Ableton > Projects` - again this makes restoring from backups or working on another computer if necessary a fair amount easier.
* Use `File > Collect All & Save` at the end of each sessions. That makes sure all the files you need are self-contained in your Project folder. That makes it easy to zip-up the entire folder and share for collaboration with everything needed within, and your main system backups should then include all that.
* Get a backup system for your main computer hard drive at least. Backup external drives too if you couldn't bare to re-download all your 3rd-party resources again in the event of loss or epic fail
* Time Machine on Mac is good if you have a spare external drive. BackBlaze is good if you want an online/off-site backup too and have a fast internet connection:
  * https://www.backblaze.com

#### Saving space

* Get to know "Racks" for Drums, Instruments, Audio Effects - they also help with focus and organisation that anyone should appreciate if you are collaborating, and using Racks will make it possible to perform the tracks live with midi-controllers if you take it to stage and screen.

# Comments, Questions, Feedback & Requests

Open a new ticket or discuss in existing ones under GitLab > Issues here:

https://gitlab.com/ableton/ableton-user-library/issues

# Collaboration

**If you're also an utter geek and want to help evolve these resources** - otherwise skip to the credits & links below...

If you want to suggest improvements to this file repository upstream, the simplest way is using GitHub Desktop & Atom for text editing:

* https://desktop.github.com
* https://atom.io

All these text files are best read in the free Atom application anyway - yes it's; free, safe and a bit geeky but probably nicer than your operating systems default text-editor for colourful reading and editing.

It should be reasonably self-explanatory and Google-able to figure out, for those that way inclined that can RTFM - otherwise let me know if you think it needs more instructions on that here.

Suggested folder structure if you want to collaborate on this repo and send Merge Requests upstream or easily Pull the latest version via GitHub Desktop:

* Copy just the `User Library - GitLab` folder to `~User > Music > Ableton > User Library - GitLab` if you're not collaborating and just want to download the latest .zip from here to overwrite that occasionally, and have it alongside your own *User Library* for reference and copying files *from* - just be careful to stick to using your default `~User > Music > Ableton > *User Library*` within Ableton for storing your own presets and just add `~User > Music > Ableton > User Library - GitLab` via *Places > Add Folder...*.
* `~User > GitLab > ableton-user-library` for the clean clone of this repo if you have synched with GitLab using GitHub Desktop, and are creating *Branches* to *Push* upstream.
* `~User > Music > Ableton > User Library` is the standard Ableton folder structure (unless you've moved it and relocated that in Ableton *Preferences*), which you should definitely keep separate from this synching `ableton-user-library` folder to avoid any accidental changes or losses, which you won't have if you've read all this.
* Clear as a crystal?

## Etiquette

It should be reasonably self-explanatory and Googleable to figure out the typical Git workflow for those that way inclined, otherwise let me know if you think it needs more instructions on that here.

* The etiquette for submissions will be that you create a *Branch*, then switch to that *Branch* in *GitHub Desktop* before making and saving your amends. Then *Push* a *Merge Request* to share upstream for consideration or discussion. Name your branch something meaningful to what sort of changes are included, and write helpful commit comments etc.
* Let's agree to follow the rainbow colour-coding and naming conventions you find for the sake of simplicity please. If you don't follow my colours, you can exclude the `User Library - GitLab > Ableton Folder Info` folder from your local copy.

# Credits, Inspirations & Further Resources

The internet knows everything - but some places and people on the other side of it know more, and are certainly worth following and supporting.

* Salford University for my Sound & Video Electronic Engineering education
  * https://beta.salford.ac.uk/courses/undergraduate/professional-sound-and-video-technology
* Ableton - of course! Best software development I've ever had the pleasure of using. It really is a complete virtual studio with the best of old and new technologies. Take notes Adobe!
  * https://ableton.com
* Significantly inspired by the great articles from Heroic Academy
  * http://heroic.academy
* Udemy - although, only ever buy courses on-sale - great value then - and there's loads of them.
  * https://click.linksynergy.com/deeplink?id=vAcWjL6nk7g&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourses%2Fsearch%2F%3Fsrc%3Dukw%26q%3Dableton
* The more helpful people on Reddit, eg:
  * https://www.reddit.com/r/audioengineering/comments/bc6pyy/saturation_first_or_last_in_signal_chain/
  * https://www.reddit.com/r/edmproduction/comments/76w7xx/saturation_before_or_after_compression/
* GearSlutz Forum geeks
  * https://www.gearslutz.com/board/music-computers/580303-saturation-plugs-where-chain.html
* Sonic Bloom, one of many lady Ableton legends
  * https://sonicbloom.net/en/gain-staging-in-ableton-live-done-right/
* Soundtoys Artist Stories
  * https://www.soundtoys.com/artists/
* Abletunes Templates (template reference tracks are great to see how your favourite styles look as projects)
  * https://abletunes.com
* Platinum Loops
  * https://www.platinumloops.com
* Prime Loops
  * https://primeloops.com
  * https://primeloops.com/sounds.html?&software=384
* Producer Loops
  * https://www.producerloops.com
* Producer Planet
  * https://www.producerplanet.com
* Plugin Boutique (+5% Reward discounts)
  * https://www.pluginboutique.com?a_aid=5e0caa34b1843&amp;a_bid=f271f4a1
* Top Music Arts (template reference tracks are great to see how your favourite styles look as projects)
  * https://topmusicarts.com
* ADSR Sounds
  * https://www.adsrsounds.com
* Gear4music
  * https://www.gear4music.com
* Reverb.com
  * https://reverb.grsm.io/marcusquinn
* Splice
  * https://splice.com
* We Make Dance Music
  * https://www.wemakedancemusic.com/en/crew/marcusquinn
* Bass Kleph Easy Wash Out
  * https://www.basskleph.com/download-easy-wash-out
* SideBrain
  * http://www.sidebrain.net/old-school-arpeggiators/
* ELPHNT
  * https://elphnt.io
* Off The Beat
  * https://www.off-the-beat.com/free-midi-files/
* Amazon UK / Ableton
  * https://amzn.to/362q0kh
* Amazon US / Ableton
  * https://amzn.to/2NHetAK

And a few really good friends and beautiful people that know who they are, inspiring me to get back into music-making after a very long time away from it - because we're here for a good time, not a long time!

Let me know any of your own good recommendations and how you get on...
